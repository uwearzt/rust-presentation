#[derive(Debug)]
enum Color {
    None,
    Gray(u8),
    Color(u8, u8, u8),
}

fn ex_tuple() -> (u8, u8) {
  (12, 34)
}

fn ex_match(x: u8, y: u8) {
    println!("in: {}, {}", x, y);
    match (x, y) {
        (1, 1) => println!("both one"),
        (1, a) if a % 2 == 0 => println!("first parameter one, second even"),
        (1, _) => println!("first parameter one"),
        (_, _) => println!("everything else"),
      }
}

// Result with return type, Error type
fn ex_result(fail: bool) -> Result<u8, String> {
  if fail {
    return Err("xxx".to_string());
  }
  Ok(7)
}

fn main() {
    // enum
    let a = Color::None;
    let b = Color::Gray(123);
    let c = Color::Color(50, 100, 150);
    println!("{:?} - {:?} - {:?}", a, b, c);

    // tuple
    let tpl = ex_tuple();
    println!("{:?}", tpl);

    let (tpl1, tpl2) = ex_tuple();
    println!("{} - {}", tpl1, tpl2);

    // match
    ex_match(1, 1);
    ex_match(1, 2);
    ex_match(1, 3);
    ex_match(2, 1);

    // Unhandled result will get you a warning
    ex_result(true);

    // This will Panic if an Error is returned
    // If you need the result, you have to hangle the Result.
    let _x = ex_result(false).unwrap();
    let _x = ex_result(true).unwrap();
}
