//! here is some documentation for the module

/// Computes 'The Answer'
/// Don't forget the question though
///
/// # Examples
///
/// ```rust
/// use s03_tests::*;
/// let answer = the_answer();
///
/// ```
pub fn the_answer() -> i32 {
    42
}

/// This function always panics
///
/// # Examples
///
/// ```rust,should_panic
/// use s03_tests::*;
/// this_will_panic();
///
/// ```
pub fn this_will_panic() {
    panic!("Oh dear...");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ensure_the_answer_is_correct() {
        assert_eq!(the_answer(), 42);
    }

    #[test]
    #[should_panic]
    fn make_sure_the_call_panics() {
        this_will_panic();
    }
}
