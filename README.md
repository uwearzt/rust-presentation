# rust-presentation

A short Rust introduction targeted for developers in C++ and Python.
Main target group are developers without a education in software enginering,
but a bsc/msc in physics, mathematics, ...

## generate presentation

### macOS

```sh
brew install marp-cli
marp --allow-local-files --pdf rust-for-devs.md
marp --html rust-for-devs.md
```

## License

[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
[![Apache licensed](https://img.shields.io/badge/license-Apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0)
